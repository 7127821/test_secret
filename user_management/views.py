from typing import Type

from rest_framework import viewsets

from user_management.serializers import BannerSerializer, DepositSerializer
from . import models
from . import serializers

"""A file that handles requests from the client and sends them for a serializers.py file"""


class SupplierViewSet(viewsets.ModelViewSet):
    """handle a url supplier request"""
    queryset: object = models.Supplier.objects.all()

    def get_serializer_class(self) -> Type[BannerSerializer]:
        """
        Checks if the HTTP request is POST or PUT
        and according to this to which serializers function to send the request
        by use override function get_serializer_class
        :return: serializer function
        """
        if self.action == 'update':
            return serializers.UpSupplierSerializer
        return serializers.SupplierSerializer


class BannerViewSet(viewsets.ModelViewSet):
    """handle a url banner request"""
    queryset: object = models.Banner.objects.all()
    serializer_class: Type[BannerSerializer] = serializers.BannerSerializer


class DepositViewSet(viewsets.ModelViewSet):
    """handle a url deposit request"""
    queryset: object = models.Deposit.objects.all()
    serializer_class: Type[DepositSerializer] = serializers.DepositSerializer

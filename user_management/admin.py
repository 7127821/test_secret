from django.contrib import admin

from . import models

admin.site.register(models.Supplier)
admin.site.register(models.SupUser)
admin.site.register(models.Business)
admin.site.register(models.Agent)

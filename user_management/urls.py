from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from .views import SupplierViewSet, BannerViewSet, DepositViewSet

router = DefaultRouter()
router.register('supplier', SupplierViewSet, basename='supplier')
router.register('banner', BannerViewSet, basename='banner')
router.register('deposit', DepositViewSet, basename='deposit')

urlpatterns = [
    url(r'', include(router.urls)),
]

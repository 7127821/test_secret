from rest_framework import serializers

from . import models


class SupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Supplier
        fields = ("id", "permanent_id", "payment_url", "logo_background_color", "image", "name", "order", "type")
        depth = 2


class UpSupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Supplier
        fields = ("id", "permanent_id", 'payment_url', "logo_background_color", "image", "name", "order", "type")
        extra_kwargs = {'permanent_id': {'required': False}, 'payment_url': {'required': False},
                        'name': {'required': False}}


class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Banner
        fields = (
            "id", "name", 'image', "page_url", "image", "order", "text", 'position', 'duration', 'count', 'status',
            'language', 'start_date', 'end_date', 'user')


class DepositSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Deposit
        fields = ("id", "computer_ip", 'sum', "payment_type", 'date', 'busy','collector')


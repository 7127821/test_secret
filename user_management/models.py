from django.contrib.auth.models import User
from django.db import models


class Supplier(models.Model):
    payment_url = models.URLField()
    permanent_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=200)
    eng_name = models.CharField(max_length=200, default="")
    arb_name = models.CharField(max_length=200, default="")
    type = models.IntegerField(default="")
    logo_background_color = models.CharField(max_length=200, default="")
    image = models.CharField(max_length=200, default="")
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Business(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Agent(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class SupUser(models.Model):
    business = models.ForeignKey(Business, on_delete=models.CASCADE, null=True)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)


class Banner(models.Model):
    class Position(models.IntegerChoices):
        Right = 1
        Left = 2
        LaiitBox = 3

    class Language(models.IntegerChoices):
        HEBREW = 1
        ENGLISH = 2
        ARAB = 3

    class Tags(models.IntegerChoices):
        A = 1
        B = 2
        C = 3

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    image = models.CharField(max_length=200, default="")
    page_url = models.URLField(default="")
    order = models.IntegerField(default=0)
    text = models.CharField(max_length=500, default="")
    position = models.IntegerField(choices=Position.choices, default=0)
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    count = models.IntegerField(null=True)
    status = models.BooleanField(null=True)
    language = models.IntegerField(choices=Language.choices, default=Language.HEBREW)
    duration = models.IntegerField(null=True)
    tags = models.IntegerField(choices=Tags.choices, default=0)
    # distributors
    # type


class Deposit(models.Model):
    class PaymentType(models.IntegerChoices):
        CASH = 1
        CREDIT = 2

    busy = models.ForeignKey(Business, on_delete=models.CASCADE)
    computer_ip = models.CharField(max_length=100, default='')
    sum = models.FloatField()
    date = models.DateTimeField()
    collector = models.ForeignKey(Agent, on_delete=models.CASCADE)
    payment_type = models.IntegerField(choices=PaymentType.choices)
